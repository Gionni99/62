
import java.util.Collections;
import java.util.List;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica62 {
    public static void main(String[] args) {
        Time Palmera;
        Palmera = new Time();
        Jogador a1,a2,a3;
        a1=new Jogador(18,"Rodisvaldo");
        a2=new Jogador(23,"Renatinho");
        a3=new Jogador(83,"Lewandowski");
        Palmera.addJogador("Goleiro",a1);
        Palmera.addJogador("Zagueiro",a2);
        Palmera.addJogador("Atacante",a3);
        JogadorComparator o=new JogadorComparator(false,true,false);
        List<Jogador> listinha;
        listinha=Palmera.ordena(o);
        System.out.println("Escalacao:"+Palmera.getJogadores().toString());
        System.out.println(Palmera.getJogadores());
        //System.out.println(Collections.binarySearch(listinha, 23).toString());
    }
}
