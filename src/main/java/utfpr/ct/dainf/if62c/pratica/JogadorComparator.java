/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.Comparator;
import java.util.List;

/**
 *
 * @author Giovanni Bandeira
 */
public class JogadorComparator implements Comparator<Jogador> {
    boolean it,//por numero
            ni,//numero ascendente
            san;//nome ascendente

    @Override
    public int compare(Jogador o1, Jogador o2) {
        if((this.it==true || o1.nome==o2.nome) && o1.numero!=o2.numero){
            if(ni==true){
                return o1.numero-o2.numero;
            }
            else{
                return o2.numero-o1.numero;
            }
        }
        else{
            if(san==true){
                return o1.nome.compareTo(o2.nome);
            }
            else{
                return o2.nome.compareTo(o1.nome);
            }
        }
    }

    public JogadorComparator(boolean it,boolean ni,boolean san) {
        this.it=it;
        this.ni=ni;
        this.san=san;
    }
}
